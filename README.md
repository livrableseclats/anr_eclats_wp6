# Livrables WP6 ANR ECLATS

## l’école thématique CARTOLING (ANR ECLATS)

Cette école thématique, qui se tiendra à Autran (Escandille) du 29/06 au 2/07/2020, est la manifestation scientifique de clôture du projet l’ANR ECLATS « Extraction automatisée des Contenus géolinguistiques d’Atlas et analyse Spatiale : application à la Dialectologie » qui finira le 31/12/2020.

Le projet, piloté par Paule-Annick Davoine (PACTE UGA) et en partenariat avec les dialectologues de GIPSA-lab (responsable pour GIPSA-lab : Elisabetta Carpitelli), se fonde sur la collaboration entre géomaticiens, statisticiens, informaticiens spécialistes du traitement de l’image et dialectologues de Grenoble, Lyon, La Rochelle, Nice, Toulouse.

Le comité d’organisation inclut 2 chercheurs (E. Carpitelli GIPSA-lab UGA) et J-Ch. Burie (L3i Univ.La Rochelle), 2 ingénieurs d’étude (C. Chauvin-Payan et J-P. Lai UGA GIPSA-lab) et un doctorant (C. Chagnaud UGA LIG-GIPSA-lab).

L’objectif principal de l’école est de former un public spécialiste du traitement et de l’analyse des données linguistiques aux meilleures solutions pour l’étude et la représentation des données spatiales, ainsi qu’un public spécialiste du traitement et de l’information géographique aux méthodologies de traitement et d’analyse des données linguistiques.

Les participants (25) seront les premiers utilisateurs des nouveaux outils permettant un renouvellement des pratiques nécessaire.

Aucune autre formation n’existe en géomatique ou géographie, adaptée aux besoins et aux contraintes méthodologiques des spécialistes de la diffusion des données linguistiques dans l’espace géographique.
La formation inédite proposée se fondera sur les enseignements de spécialistes (8) d’informatique, statistique et linguistique, apportant des expériences diversifiées d’extraction de données textuelles, de traitement, d’analyse et de représentation spatiale de données. L’enjeu principal de l’école thématique est d’optimiser l’efficacité (simplicité et gain de temps) des pratiques en géographie linguistique, en précisant vocation, possibilités et meilleure utilisation des logiciels existants, et des nouveaux outils d’extraction et saisie des contenus graphiques, ainsi que les procédures de manipulation et de présentation des données linguistiques dans une aire géographique. 

L’école a reçu le soutien des sections 07 et 34 du CNRS.
